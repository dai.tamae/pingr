export const state = () => ({
    token: ""
})
  
export const mutations = {
    registerToken(state,token) {
        state.token=token;
    }
}